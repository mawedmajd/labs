#pragma once
#include "GeometricTools.h"
#include <GLFW/glfw3.h>
#include <iostream>

class Lab2Application {
    Lab2Application();

    virtual unsigned int ParseArguments(int argc, char** argv);
    virtual unsigned Init();
    virtual unsigned Run();
}