// #include area (SECTION 1)

#include <iostream>
#include <string>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

void processInput(GLFWwindow* window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

int main(int argc, char** argv) {
	bool console, ifTriangle, ifSquare;
	int width, height;
	float currentTime, sinTime, cosTime, rsinTime, rcosTime, hsinTime;

	// Command-line parsing code (SECTION 2)

	try {
		// cmd(printed last in the help text, delimiter, version number, disable default arguments)
		TCLAP::CmdLine cmd("Command description message", ' ', "0.9", false);

		// SwitchArg is a boolean. (flag, name, description, add to command line object, default value)
		TCLAP::SwitchArg consoleSwitch("c", "console", "print \": console\"", cmd, false);
		TCLAP::SwitchArg triangleSwitch("t", "triangle", "print triangle", cmd, true);
		TCLAP::SwitchArg squareSwitch("s", "square", "print square", cmd, true);


		TCLAP::ValueArg<int> widthArg("w", "width", "configure the width of the window", false, 1000, "int");
		TCLAP::ValueArg<int> heightArg("h", "height", "configure the height of the window", false, 750, "int");

		cmd.add(widthArg);
		cmd.add(heightArg);

		// Parse the argv array.
		cmd.parse(argc, argv);

		console = consoleSwitch.getValue();
		ifTriangle = triangleSwitch.getValue();
		ifSquare = squareSwitch.getValue();
		width = widthArg.getValue();
		height = heightArg.getValue();

	}
	catch (TCLAP::ArgException& e) {
		std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
	}

	// GLFW initialization code (SECTION 3)

	if (!glfwInit()) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}

	// OpenGL initialization code (SECTION 4)

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	GLFWwindow* window = glfwCreateWindow(width, height, "HelloOpenGL", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}
	// tell GLFW to make the context of our window the main context on the current thread
	glfwMakeContextCurrent(window);

	// pass glad the function to load the OpenGL function pointers
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	// OpenGL data transfer code (SECTION 5)

	float triangle[6 * 3] = {
		-0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f
	};

	float square[6 * 3 * 2] = {
		-0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,

		-0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f
	};

	// Create a vertex array (VAO)
	// VAO stores all of the state needed to supply vertex data
	GLuint vertexArrayId[2];   //unsigned int
	glGenVertexArrays(2, vertexArrayId);

	// Create vertex buffers (VBO)
	// VBO is a buffer used to transfer the	data from CPU to GPU
	GLuint vertexBufferId[2];  //unsigned int
	glGenBuffers(2, vertexBufferId);

	// triangle
	// set active VAO
	glBindVertexArray(vertexArrayId[0]);
	// set active VBO
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]);
	// Populate the vertex buffer, transfer the data
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_DYNAMIC_DRAW);
	// set the layout of the bound buffer
	// (Attribute Index, conponents, type, normalize, size of one component, ptr to the current element of the component) 
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (void*)12);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1); 

	// square
	glBindVertexArray(vertexArrayId[1]);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square), square, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (void*)12);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);


	// vertex shader code
	const std::string vertexShaderSrc = R"(
#version 410 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
out vec3 vertexColor;

void main() 
{
gl_Position = vec4(position, 1.0);
vertexColor = color;
}	
)";

	// fragment shader code
	const std::string fragmentShaderSrc = R"(
#version 410 core

out vec4 fragColor;
in vec3 vertexColor;

void main()
{
fragColor = vec4(vertexColor, 1.0);
}
)";

	// compile the vertex shader
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vss = vertexShaderSrc.c_str();
	glShaderSource(vertexShader, 1, &vss, NULL);
	glCompileShader(vertexShader);
	// check for shader compile errors
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// compile the fragment shader
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fss = fragmentShaderSrc.c_str();
	glShaderSource(fragmentShader, 1, &fss, NULL);
	glCompileShader(fragmentShader);
	// check for shader compile errors
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// create a shader program
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	// Application loop code (SECTION 6)

	while (!glfwWindowShouldClose(window))
	{
		currentTime = glfwGetTime();
		sinTime = sin(currentTime) / 2 + 0.5;
		cosTime = cos(currentTime) / 2 + 0.5;
		rsinTime = -sin(currentTime) / 2 + 0.5;
		rcosTime = -cos(currentTime) / 2 + 0.5;
		hsinTime = sin(currentTime * 2) / 2 + 0.5;

		glClearColor(hsinTime, hsinTime, hsinTime, hsinTime);
		glClear(GL_COLOR_BUFFER_BIT);

		if (ifSquare) {
			float newColor1[3] = { cosTime, sinTime, rsinTime };
			float newColor2[3] = { sinTime, rsinTime, rcosTime };
			float newColor3[3] = { rsinTime, rcosTime, sinTime };
			float newColor4[3] = { rcosTime, sinTime, rsinTime };
			glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[1]);
			glBufferSubData(GL_ARRAY_BUFFER, 12, 12, newColor1);
			glBufferSubData(GL_ARRAY_BUFFER, 36, 12, newColor2);
			glBufferSubData(GL_ARRAY_BUFFER, 60, 12, newColor3);
			glBufferSubData(GL_ARRAY_BUFFER, 84, 12, newColor1);
			glBufferSubData(GL_ARRAY_BUFFER, 108, 12, newColor4);
			glBufferSubData(GL_ARRAY_BUFFER, 132, 12, newColor3);

			glUseProgram(shaderProgram);
			glBindVertexArray(vertexArrayId[1]);
			glDrawArrays(GL_TRIANGLES, 0, 6);
		}
		if (ifTriangle) {
			float newColor1[3] = { cosTime, sinTime, 0.0 };
			float newColor2[3] = { sinTime, 0.0, cosTime };
			float newColor3[3] = { 0.0, cosTime, sinTime };
			glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId[0]);
			glBufferSubData(GL_ARRAY_BUFFER, 12, 12, newColor1);
			glBufferSubData(GL_ARRAY_BUFFER, 36, 12, newColor2);
			glBufferSubData(GL_ARRAY_BUFFER, 60, 12, newColor3);

			glUseProgram(shaderProgram);
			glBindVertexArray(vertexArrayId[0]);
			glDrawArrays(GL_TRIANGLES, 0, 3);
		}

		glfwSwapBuffers(window);

		if (console) {
			std::cout << "Hello OpenGL: console" << std::endl;
		}

		glfwPollEvents();
		processInput(window);
	}

	// Termination code (SECTION 7)

	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}
