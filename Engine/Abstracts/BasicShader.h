#shader vertex
#version 460 core

layout(location = 0) in vec2 position;

void main()
{
	gl_Position = vec4(position, 0.0, 1.0);
}

#shader fragment
#version 460 core

layout(location = 0) out vec2 position;

void main()
{
	fragColor = vec4(1.0, 1.0, 1.0, 1.0);
}
