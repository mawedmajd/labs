#pragma once
#include <string>
#include <GLFW/glfw3.h>
#define ASSERT(x) if(!(x)) __debugbreak();

class GLFWApplication
{
protected:
	const std::string& name;
	const std::string& version;
	int width, height;
	GLFWwindow* window;

public:
	GLFWApplication(const std::string& name_, const std::string& version_);
	~GLFWApplication();

	virtual unsigned int ParseArguments(int argc, char** argv); // Virtual function with default behavior.
	virtual unsigned int Init(); // Virtual function with defaut behavior
	virtual unsigned int Run() const = 0; // Pure virtual function, it must be redefined
};
