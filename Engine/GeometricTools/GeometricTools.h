#pragma once
#include <array>

namespace GeometricTools {
	constexpr std::array<float, 3 * 2> UnitTriangle2D = { -0.5f, -0.5f, 0.5f, -0.5f, 0.0f, 0.5f };


	constexpr std::array<float, 6 * 2> UnitSquare2D = {
		-0.5f,  0.5f,
		 0.5f,  0.5f,
		 0.5f, -0.5f,
		-0.5f,  0.5f,
		-0.5f, -0.5f,
		 0.5f, -0.5f
	};

	
}
