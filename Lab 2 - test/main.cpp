#include <iostream>
#include <string>
#include <tclap/CmdLine.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>


void processInput(GLFWwindow* window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

int main(int argc, char** argv) { 
	int width = 600;
	int height = 600;

	if (!glfwInit()) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);

	GLFWwindow* window = glfwCreateWindow(width, height, "HelloCB", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return EXIT_FAILURE;
	}
	// tell GLFW to make the context of our window the main context on the current thread
	glfwMakeContextCurrent(window);

	// pass glad the function to load the OpenGL function pointers
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

	float square[6 * 3 * 2] = {
		-0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,

		-0.5f,  0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f,
		 0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 0.0f
	};

	GLuint vertexArrayId;
	glGenVertexArrays(1, vertexArrayId);


	GLuint vertexBufferId;
	glGenBuffers(1, vertexBufferId);


	glBindVertexArray(vertexArrayId);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
	glBufferData(GL_ARRAY_BUFFER, sizeof(square), square, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (void*)12);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);


	const std::string vertexShaderSrc = R"(
#version 460 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;
out vec3 vertexColor;

void main() 
{
gl_Position = vec4(position, 1.0);
vertexColor = color;
}	
)";

	// fragment shader code
	const std::string fragmentShaderSrc = R"(
#version 460 core

out vec4 fragColor;
in vec3 vertexColor;

void main()
{
fragColor = vec4(vertexColor, 1.0);
}
)";

	// compile the vertex shader
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vss = vertexShaderSrc.c_str();
	glShaderSource(vertexShader, 1, &vss, NULL);
	glCompileShader(vertexShader);
	// check for shader compile errors
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// compile the fragment shader
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fss = fragmentShaderSrc.c_str();
	glShaderSource(fragmentShader, 1, &fss, NULL);
	glCompileShader(fragmentShader);
	// check for shader compile errors
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// create a shader program
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	while (!glfwWindowShouldClose(window))
	{
		glClearColor(1,1,1,1);
		glClear(GL_COLOR_BUFFER_BIT);


		glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
		glUseProgram(shaderProgram);
		glBindVertexArray(vertexArrayId);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		glfwSwapBuffers(window);

		glfwPollEvents();
		processInput(window);
	}

	// Termination code (SECTION 7)

	glfwDestroyWindow(window);
	glfwTerminate();

	return EXIT_SUCCESS;
}

